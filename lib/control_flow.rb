# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  uppercase_only = String.new
  str.split("").each do |letter|
    uppercase_only << letter if letter == letter.upcase
  end
  uppercase_only
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str_array = str.split("")

  #if odd
  return str_array[str_array.length / 2] if str_array.length.odd?

  #if even
  "#{str_array[str_array.length / 2 - 1]}#{str_array[str_array.length / 2]}"
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowel_count = 0

  "aeiouAEIOU".split("").each { |vowel| vowel_count += str.split("").count(vowel)}

  vowel_count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  factorial = 1
  (1..num).to_a.reverse.each { |number| factorial *= number }

  factorial
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined_str = String.new
  arr.each_with_index do |letter, idx|
    joined_str << "#{letter}#{separator}" if idx < arr.length - 1
    joined_str << "#{letter}" if idx == arr.length - 1
  end

  joined_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = String.new
  str.split("").each_with_index do | letter, idx |
    weird_str << letter.downcase if idx.even? || idx == 0
    weird_str << letter.upcase if idx.odd?
  end

  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str_array = str.split(" ")
  str_array.map! { |word| ( word.length >= 5 ? word.reverse : word) }
  str_array.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fizzbuzz_arr = Array.new
  (1..n).to_a.each do |num|
    if num % 3 == 0 && num % 5 == 0
      fizzbuzz_arr << "fizzbuzz"
    elsif num % 3 == 0
      fizzbuzz_arr << "fizz"
    elsif num % 5 == 0
      fizzbuzz_arr << "buzz"
    else fizzbuzz_arr << num
    end

  end
  fizzbuzz_arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  idx = arr.length - 1
  reversed_arr = Array.new

  arr.each do |ele|
    reversed_arr[idx] = ele
    idx -= 1
  end
  reversed_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2...num).to_a.each { |factor| return false if num % factor == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num) # LOOK OVER THIS IF YOU CAN
  (1..num).select { |factor| factor if num % factor == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |p_factor| p_factor if prime?(p_factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_count = 0
  arr.each { |num| odd_count += 1 if num.odd? }

  if odd_count == 1
    arr.each { |num| return num if num.odd? }
  else
    arr.each { |num| return num if num.even? }
  end
end
